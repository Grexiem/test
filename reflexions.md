---
title: "Réflexions"
order: 1
in_menu: true
---
## Comment bien s'informer ?

### Youtube
Site incontournable pour qui cherche une vidéo.
Ce site est celui que j'utilise le plus car il apparaît pour moi comme une sorte de bibliothèque de données vidéos. Le problème ? C'est une bibliothèque composée d'un couloir qui change systématiquement en fonction du livre choisi.
Même si le livre choisi n'a pas plu, celui-ci aura un impact sur les prochains livres proposés.

Comment donc faire pour que ce couloir soit le moins biaisé possible.
Entrer dans un tunnel est si facile, et pourtant je ne pense pas complètement être entré dans l'un d'entre eux.
Cela fait maintenant plus de 12 ans que j'évolue avec cet outil et bien sûr, j'ai eu des passions différentes qui se sont traduites par des envies différentes de contenu. Est-ce mal que l'algorithme m'ait proposé des vidéos sur le sujet ?

D'un côté on pourrait dire que celui-ci laisse néanmoins un choix entre plusieurs vidéos. Ce qu'un ami, un professeur, un collègue n'aurait pas forcément fait car souvent, pour convaincre dans un court temps disponible, on ne peut proposer qu'une ou plusieurs sources.

D'un autre côté ce choix n'est pas fait seulement pour moi, mais aussi pour nombre d'utilisateurs. Ce qui peut entraîner des risques de pensée unique, de mauvaise compréhension ou bien tout simplement ce que tous les médias aime parler, de fake news.

Le problème est-il donc au niveau de la plateforme ? de l'utilisateur ? des créateurs ?

Une réponse simple serait les 3, mais en particulier la plateforme.
Pourquoi cela ?
Parce que la plateforme est irremplaçable et elle a besoin d'être au mieux rentable, au pire profiter à d'autres secteurs pour qu'un bénéfice pécuniaire puisse en sortir.

#### Une plateforme libre ou bien au minima provenant du service public serait-elle une solution ? 