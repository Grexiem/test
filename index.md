---
title: "Ma page de recommandations"
order: 0
in_menu: true
---
Voici les logiciels libres que je souhaite utiliser et promouvoir

# Scribouilli

Ce logiciel avec lequel ce site est créé

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Scribouilli.png">
    </div>
    <div>
      <h2>Scribouilli</h2>
      <p>Créer votre petit site facilement !</p>
      <div>
        <a href="https://framalibre.org/notices/scribouilli.html">Vers la notice Framalibre</a>
        <a href="https://scribouilli.org/">Vers le site</a>
      </div>
    </div>
  </article> 

# Signal

Pour la famille ça marche très bien.
Très bonne alternative à Whatsapp


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Signal.png">
    </div>
    <div>
      <h2>Signal</h2>
      <p>Application de messagerie et téléphonie mobile respectueuse de la vie privée.</p>
      <div>
        <a href="https://framalibre.org/notices/signal.html">Vers la notice Framalibre</a>
        <a href="https://signal.org">Vers le site</a>
      </div>
    </div>
  </article> 

Pour les jeux sous Linux, Lutris fonctionne très bien, même si Steam a bien réussi à porter ses jeux sur Linux maintenant.


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Lutris.png">
    </div>
    <div>
      <h2>Lutris</h2>
      <p>La plateforme de jeu qui réunit toutes les possibilités pour jouer sous Linux.</p>
      <div>
        <a href="https://framalibre.org/notices/lutris.html">Vers la notice Framalibre</a>
        <a href="https://lutris.net/">Vers le site</a>
      </div>
    </div>
  </article> 